#include <stdio.h>
#include <string.h>

typedef void(*init_matrix)(struct Matrix *, int rows, int cols);
typedef void(*init_vector)(struct Vector *, int rows); 
typedef void(*addition)(struct Matrix *, struct Matrix *);
typedef void(*multiplication)(struct Matrix *, struct Matrix *);
typedef void(*L1norm)(struct Matrix *);


typedef struct Matrix{
	int rows;
	int cols;
	int **array;
	L1norm *vTable;
	multiplication multiplyFunc;
	addition addFunc;
	init_matrix init;
}MbyN_Matrix;

typedef struct Vector{
	MbyN_Matrix vec;
	init_vector init;
}Mby1_Vector;


void matrixAdd(struct Matrix *A, struct Matrix *B){
	
	
	if ((A->row == B->row) && (A->col == B->col)){
		int result[A->rows][A->cols];
		for (int i = 0; i < A->row; i++){
			for (int j = 0; j < A->col; j++){
				result[i][j]=A->vec[i][j] + B->vec[i][j];
			}
		}
		for (int i = 0; i < A->rows ; i++) {
			for (int j = 0; j < B -> cols; j++){
				printf("%d  ", result[i][j]);
			}
			printf("\n");
		}
	}
	else {
		printf("Marices could not be added.");
	}
}

void matrixMultiply(struct Matrix *A, struct Matrix *B)
{
	int temp = 0;
	int result[A->rows][B->cols];

	if (A -> cols == B -> rows) {
		for (int i = 0; i < A -> rows; i++) {
			for (int j = 0; j < B -> cols; j++) {
				for (int k = 0; k < B -> rows; k++) {
					temp = temp + (A->array[i][k]) * (B->array[k][j]);
				}
				result[i][j] = temp;
				temp = 0;
			}
		}

		for (int i = 0; i < A->rows ; i++) {
			for (int j = 0; j < B -> cols; j++){
				printf("%d  ", result[i][j]);
			}
			printf("\n");
		}
	}
	
	else{
		printf("Matrices could not be multiplied.\n");
	}
}
void matrixL1Norm (struct Matrix *A){
	if ((A->rows != 0) && (A->cols != 0)) {
		int var1=0,var2=0;
		for (int i = 0; i < A->cols; i++) {
			var1 = 0;
			for (int j = 0; j < A->rows; j++) {
				var1 += A->array[i][j];
			}
			if(var1>var2){
				var2=var1;
			}
		}
		printf("L1 Norm of matrix is %d\n", var2);
	}
	else {
		printf("Cant find L1Norm for Matrix\n");
	}
}

void vectorL1Norm (struct Matrix *A){
    int var=0;
    for(int i = 0 ; i< A->rows; i++){
        var+=A->array[i][0];
    }
	printf("L1 Norm of vector is %d\n", var);
}


void matrixInit(struct Matrix *A, int rows, int cols){
	MbyN_Matrix mat;
	mat.rows = rows;
	mat.cols = cols;
	mat.multiplyFunc = matrixMultiply;
	mat.printthis = printMatrix;
	mat.addFunc = matrixAdd;
	mat.array = malloc(rows * sizeof(int *));

	for (int i = 0; i < rows; i++)
	{
		mat.array[i] = malloc(cols * sizeof(int));
	}
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			mat.array[i][j] = 1;
		}
	}
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			printf("%d  ", mat.array[i][j]);
		}
		printf("\n");
	}

	*A = mat;
}

void vectorInit(struct Vector *A, int rows){
	Mby1_Vector v;
	v.vec.init = matrixInit;
	v.vec.init(&v.vec, rows, 1);
	*A = v;
}

void main() {
	L1norm *vTableMatrix = malloc(sizeof(L1norm));
    vTableMatrix[0] = &matrixL1Norm;

	MbyN_Matrix *m;
	MbyN_Matrix *n;
	MbyN_Matrix *v;
	MbyN_Matrix *w;
	printf("m: \n");
	m = (MbyN_Matrix*)malloc(sizeof(MbyN_Matrix));
	m->init = matrixInit;
	m->init (m, 5, 5);
	printf("n: \n");
	n = (MbyN_Matrix*)malloc(sizeof(MbyN_Matrix));
	n->init = matrixInit;
	n->init (m, 5, 5);
	printf("m+n: \n");
	m->addFunc(m, n);
	printf("m*n: \n");
	m->multiplyFunc(m, n);
	printf("L1 Norm: \n");
	m->vTable = vTableMatrix;
	m->vTable[0](m);
	
	printf("v: \n");
	v = (MbyN_Matrix*)malloc(sizeof(Mby1_Vector));
	v->init = vectorInit;
	v->init (v, 5);
	printf("w: \n");
	w = (MbyN_Matrix*)malloc(sizeof(Mby1_Vector));
	w->init = vectorInit;
	w->init (w, 5);
	printf("v+w: \n");
	v->addFunc(v, w);
	printf("v*w: \n");
	v->multiplyFunc(v, w);
	
}